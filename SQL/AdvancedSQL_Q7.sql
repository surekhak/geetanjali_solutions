DELIMITER $$

DROP FUNCTION IF EXISTS `showPrevMonths`$$



CREATE FUNCTION `showPrevMonths`(checkDate Date) returns varchar(90)



BEGIN

	declare strtTemp varchar(40);
	declare strOutput varchar(90);
	declare i int default 0;
		
		set strOutput="";
		set i=1;

	if (select day(checkDate) && (select MONTH(checkDate)))
	then
		set strtTemp=MONTH(checkDate);
		while strtTemp>=i
		do

			set strOutput=concat(strOutput,(select (MonthName) from getMonth where no=i),"\n");
			set i=i+1;
		end while;
	else
		set strOutput="Please Enter the date in yy-mm-dd format";

	end if;
	
		
		return strOutput;
END$$

DELIMITER ;

select showPrevMonths("2014/4/3");




DELIMITER $$
DROP PROCEDURE IF EXISTS sp_geetanjali_Odd_Even;
CREATE PROCEDURE `sp_geetanjali_Odd_Even`()

BEGIN

	SET @rank=0;

	

	SELECT Odd_Dist_Id, Odd_Dist_Name, Even_Dist_Id, Even_Dist_Name

	FROM(

		(SELECT @rank:=@rank+1 AS rank,district_id AS Odd_Dist_Id,district_name AS Odd_Dist_Name

			FROM tbl_geetanjali_district, (SELECT @rank := 0) r

		WHERE MOD(district_id,2)=1) AS ODD LEFT JOIN

		(SELECT @Erank:=@Erank+1 AS rank,district_id AS Even_Dist_Id,district_name AS Even_Dist_Name

			FROM tbl_geetanjali_district, (SELECT @Erank := 0) r

		WHERE MOD(district_id,2)=0) AS EVEN

		ON ODD.rank=EVEN.rank

	);

END
$$



DELIMITER ;


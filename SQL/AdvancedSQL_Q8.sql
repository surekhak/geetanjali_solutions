



DELIMITER $$


DROP FUNCTION IF EXISTS fn_geetanjali_comma;

create function fn_geetanjali_comma(District_Nm varCHAR(50))

returns TEXT


BEGIN

 	declare return_val varchar(100);

 	SET return_val='';

 	if District_Nm ='' then

 		set return_val= 'please enter district name';

 	else

	 	SELECT GROUP_CONCAT(Population) into return_val FROM tbl_geetanjali_district where district_name=District_Nm group by District_Nm;

	 	if return_val ='' then

 			set return_val= 'please enter valid district name';	

 		END IF;

	End if;	

 	return return_val;

END



$$



DELIMITER ;



    select fn_geetanjali_comma("satara");

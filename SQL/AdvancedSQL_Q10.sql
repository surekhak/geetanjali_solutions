

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_geetanjali_EncryptWithKey;

CREATE PROCEDURE `sp_geetanjali_EncryptWithKey`(IN `entext` varchar(50), IN `keyval` varchar(8))


begin
declare des varchar(50);
declare len int;
set len=length(keyval);
    if keyval regexp '[A-Za-z]' && keyval regexp '[0-9]' && length(keyval)>=6 && length(keyval)<9

	then
        
	  
	    begin
	 
	     if(EXISTS(select Message from tbl_Encryption where Message=entext and Keyval=keyval))
                       then
                         select txtDecrypt from tbl_Encryption where Message=entext and keyval=keyval;
                   
        elseif(EXISTS(select Message from tbl_Encryption where txtDecrypt=entext and Keyval=keyval)) then
     
                      select Message from tbl_Encryption where txtDecrypt=entext and keyval=keyval;
          else
                     
						set des=AES_ENCRYPT(entext,keyval);
                
                   insert  into tbl_Encryption values(entext,keyval,des);
                   
             end if;  
				 end;  
	else
	     select 'key length must be between 6-8' as error;
         
     
                
          end if;
end
$$







DELIMITER ;


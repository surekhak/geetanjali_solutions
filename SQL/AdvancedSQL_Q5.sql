


DELIMITER $$

DROP PROCEDURE IF EXISTS sp_geetanjali_District_Nm_As_PerCount;

CREATE PROCEDURE `sp_geetanjali_District_Nm_As_PerCount`(IN `RCount` INT)



BEGIN

	declare result int;

	set result=0;

	

	If RCount= 0  then

		select 'Please Enter Valid Rank' AS 'Result';

	else

		SELECT district_name

		FROM (

			SELECT district_name,COUNT(*) as Nmcount 

			FROM tbl_geetanjali_district 

			GROUP BY district_name 

			ORDER BY Nmcount

			) AS tbl_Count

		Where Nmcount=RCount

		;

	end if;

	SET result= FOUND_ROWS();

		

	if result = 0 then

		select 'Record not found' AS 'Result';

	end if;

END



$$



DELIMITER ;


call sp_geetanjali_District_Nm_As_PerCount("1");
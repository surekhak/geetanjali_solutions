
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_geetanjali_amortization;

CREATE PROCEDURE `sp_geetanjali_amortization`(principle int,rate float,period int)

BEGIN

	declare PeriodInMonths int; 
	declare interestRate float;
	declare amount float;
	declare prevBalance float;
	declare i int default 0;
	set i=1;

	if not principle regexp '^[0-9]' 
	then
		select 'Please Enter the appropriate principle amount';

	else
	if length(principle)<=0 || length(rate)<=0 || length(period)<=0 || principle=' ' || rate=' ' || period=' '
	then
	if length(principle)<=0 || principle=' '
	then 
		select 'Please Enter the principle Amount';
	else
	if length(rate)<=0 || rate=' '
	then 
		select 'Please Enter the rate';
	else
	if length(period)<=0 || period=' '
	then 
		select 'Please Enter the no of years';

	end if;
	end if;
	end if;

	else 
	if length(substring_index(period,'.',1))>2 
	then
		select 'Please Enter the no. of years upto 2 digits';
	else
	if length(substring_index(rate,'.',1))<3 && length(substring_index(rate,'.',-1))<3
	then
		set periodInMonths=period*12;
		set interestRate=(rate/1200);
		set amount=(principle*interestRate*POWER(1+interestRate,periodInMonths))/(power(1+interestRate,periodInMonths)-1);
	insert into tbl_geetanjali_amortization values(0,0,0,0,principle);
		set prevBalance=principle;
	while i<=periodInMonths
	do
			if i=periodInMonths
			then
					insert into tbl_geetanjali_amortization values(i,amount,prevBalance*interestRate,amount-(prevBalance*interestRate),round(prevBalance-(amount-(prevBalance*interestRate))));
					set prevBalance=prevBalance-(amount-(prevBalance*interestRate));
					set i=i+1;
			else
					insert into tbl_geetanjali_amortization values(i,amount,prevBalance*interestRate,amount-(prevBalance*interestRate),prevBalance-(amount-(prevBalance*interestRate)));
					set prevBalance=prevBalance-(amount-(prevBalance*interestRate));
					set i=i+1;
			end if;
	end while;
	select * from tbl_geetanjali_amortization;
	else
			select 'please enter appropriate rate upto 2 digits and upto 2 decimal places.';

end if;
end if;
end if;
end if;
END
$$


DELIMITER ;

call sp_geetanjali_amortization("2000",7.5,'1');

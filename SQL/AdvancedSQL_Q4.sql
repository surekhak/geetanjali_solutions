
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_geetanjali_ranking;


CREATE  PROCEDURE `sp_geetanjali_ranking`(IN `Dist_Name` VARCHAR(50), IN `PRank` INT)

	
BEGIN

	declare result int;

	set result=0;

	

	if Dist_Name = '' then

		select 'Please Enter District Name' AS 'Result';

	ElseIf PRank= 0  then

		select 'Please Enter Valid Rank' AS 'Result';

	else

		SELECT    district_name,Population

		FROM (

			SELECT    district_name,Population,

			          @curRank := @curRank + 1 AS rank

			 FROM     (

							SELECT    district_name,Population

							from

									tbl_geetanjali_district p, (SELECT @curRank := 0) r

							where district_name = Dist_Name

							ORDER BY  Population DESC

							) AS tbl_disp

				) AS tbl_rank

		where rank=PRank

		;

		SET result= FOUND_ROWS();

		

		if result = 0 then

		
			select 'Record not found' AS 'Result';

		end if;

	end if;



END


$$



DELIMITER ;


call sp_geetanjali_ranking("Satara",3);





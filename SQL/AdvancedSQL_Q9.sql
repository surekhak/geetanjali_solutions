﻿



DELIMITER $$



DROP FUNCTION IF EXISTS fn_geetanjali_reverse;



create function fn_geetanjali_reverse(Input_Str VARCHAR(100))



returns TEXT



BEGIN

	DECLARE Str_Len int;

	DECLARE i int;

	DECLARE asci int;

	DECLARE temp char;

	DECLARE Output_Str varchar(100);

	SET i=0;

	SET temp='';

	SET Output_Str='';

	SET asci=0;

	SET Str_Len=length(Input_Str);

	if Input_Str = '' then

		SET Output_Str='Please Enter Some String'; 

	ELSE

	

		while i<=Str_Len do

			set asci= ASCII(SUBSTRING(Input_Str, i, 1));

			if (asci>=48 AND asci<=57) then

				SET asci= 57-(asci-48);

			

			ELSEIF (asci>=65 AND asci<=90) then

				SET asci=90-(asci-65);

			

			ELSEIF (asci>=97 AND asci<=122) then

				SET asci=122-(asci-97);

			
			ELSEIF (asci<128) then

				SET asci=(asci+128);

				

			ELSEIF (asci>=128) then

				SET asci=(asci - 128);

			

			END IF;

			SET Output_Str = CONCAT(Output_Str,CHAR(asci));

			SET i=i+1;

		END WHILE;

	END IF;

	SET Output_Str = TRIM(LEADING '€' FROM Output_Str );

	return Output_Str;

END
$$



DELIMITER ;



    select fn_geetanjali_reverse("geetanjali");